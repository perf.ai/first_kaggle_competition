Hello!

Below you can find a outline of how to reproduce my solution for the Predict Future Sales competition.
If you run into any trouble with the setup/code or have any questions please contact me at ghostregionpanayiotis@gmail.com

# ARCHIVE CONTENTS
SalesPredComp.ipynb          : original kaggle model upload - contains original code, additional training examples, corrected labels, etc
datasets_EDA.ipynb           : Exploratory data analysis for the datasets
comp_preds                   : model predictions
pickle_files                 : pickle files to load second level features

# HARDWARE: (The following specs were used to create the original solution)
## CPU info
processor	: 0
vendor_id	: GenuineIntel
cpu family	: 6
model		: 79
model name	: Intel(R) Xeon(R) CPU @ 2.20GHz
stepping	: 0
microcode	: 0x1
cpu MHz		: 2200.152
cache size	: 56320 KB
physical id	: 0
siblings	: 2
core id		: 0
cpu cores	: 1
apicid		: 0
initial apicid	: 0
fpu		: yes
fpu_exception	: yes
cpuid level	: 13
wp		: yes
flags		: fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 ss ht syscall nx pdpe1gb rdtscp lm constant_tsc rep_good nopl xtopology nonstop_tsc cpuid tsc_known_freq pni pclmulqdq ssse3 fma cx16 pcid sse4_1 sse4_2 x2apic movbe popcnt aes xsave avx f16c rdrand hypervisor lahf_lm abm 3dnowprefetch invpcid_single ssbd ibrs ibpb stibp fsgsbase tsc_adjust bmi1 hle avx2 smep bmi2 erms invpcid rtm rdseed adx smap xsaveopt arat md_clear arch_capabilities
bugs		: cpu_meltdown spectre_v1 spectre_v2 spec_store_bypass l1tf mds swapgs taa
bogomips	: 4400.30
clflush size	: 64
cache_alignment	: 64
address sizes	: 46 bits physical, 48 bits virtual
power management:

processor	: 1
vendor_id	: GenuineIntel
cpu family	: 6
model		: 79
model name	: Intel(R) Xeon(R) CPU @ 2.20GHz
stepping	: 0
microcode	: 0x1
cpu MHz		: 2200.152
cache size	: 56320 KB
physical id	: 0
siblings	: 2
core id		: 0
cpu cores	: 1
apicid		: 1
initial apicid	: 1
fpu		: yes
fpu_exception	: yes
cpuid level	: 13
wp		: yes
flags		: fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 ss ht syscall nx pdpe1gb rdtscp lm constant_tsc rep_good nopl xtopology nonstop_tsc cpuid tsc_known_freq pni pclmulqdq ssse3 fma cx16 pcid sse4_1 sse4_2 x2apic movbe popcnt aes xsave avx f16c rdrand hypervisor lahf_lm abm 3dnowprefetch invpcid_single ssbd ibrs ibpb stibp fsgsbase tsc_adjust bmi1 hle avx2 smep bmi2 erms invpcid rtm rdseed adx smap xsaveopt arat md_clear arch_capabilities
bugs		: cpu_meltdown spectre_v1 spectre_v2 spec_store_bypass l1tf mds swapgs taa
bogomips	: 4400.30
clflush size	: 64
cache_alignment	: 64
address sizes	: 46 bits physical, 48 bits virtual
power management:

## Disk Info
Filesystem      Size  Used Avail Use% Mounted on
overlay         108G   31G   72G  31% /
tmpfs            64M     0   64M   0% /dev
tmpfs           6.4G     0  6.4G   0% /sys/fs/cgroup
shm             5.9G     0  5.9G   0% /dev/shm
tmpfs           6.4G   12K  6.4G   1% /var/colab
/dev/sda1       114G   33G   82G  29% /etc/hosts
tmpfs           6.4G     0  6.4G   0% /proc/acpi
tmpfs           6.4G     0  6.4G   0% /proc/scsi
tmpfs           6.4G     0  6.4G   0% /sys/firmware

## Memory info
MemTotal:       13333556 kB
MemFree:        10702648 kB
MemAvailable:   12489428 kB
Buffers:           71292 kB
Cached:          1874196 kB
SwapCached:            0 kB
Active:           705564 kB
Inactive:        1668600 kB
Active(anon):     408132 kB
Inactive(anon):      316 kB
Active(file):     297432 kB
Inactive(file):  1668284 kB
Unevictable:           0 kB
Mlocked:               0 kB
SwapTotal:             0 kB
SwapFree:              0 kB
Dirty:               328 kB
Writeback:             0 kB
AnonPages:        428664 kB
Mapped:           224740 kB
Shmem:               924 kB
Slab:             160632 kB
SReclaimable:     123936 kB
SUnreclaim:        36696 kB
KernelStack:        3472 kB
PageTables:         5268 kB
NFS_Unstable:          0 kB
Bounce:                0 kB
WritebackTmp:          0 kB
CommitLimit:     6666776 kB
Committed_AS:    2469616 kB
VmallocTotal:   34359738367 kB
VmallocUsed:           0 kB
VmallocChunk:          0 kB
Percpu:              920 kB
AnonHugePages:         0 kB
ShmemHugePages:        0 kB
ShmemPmdMapped:        0 kB
HugePages_Total:       0
HugePages_Free:        0
HugePages_Rsvd:        0
HugePages_Surp:        0
Hugepagesize:       2048 kB
Hugetlb:               0 kB
DirectMap4k:       74940 kB
DirectMap2M:     6215680 kB
DirectMap1G:     9437184 kB

# SOFTWARE (python packages are detailed separately in `requirements.txt`):
Python 3.6.9

# DATA SETUP (assumes the [Kaggle API](https://github.com/Kaggle/kaggle-api) is installed)
# below are the shell commands used in each step, as run from the top level directory
mkdir -p data/stage1/
cd data/stage1/
kaggle competitions download -c competitive-data-science-predict-future-sales
> However you do not need to download them as the notebook will use the datasets from a gitlab repository.

# MODEL BUILD: There are three options to produce the solution.
1) Use stacking ensemble model (steps 0-5)
    a) runs in 1-2 hours
    b) uses original datasets
    c) more accurate predictions
2) Use simplyfied model (steps 0-2, 6)
    a) runs in about 20 minutes
    b) uses original datasets
3) Load second level features from pickle files (0,1,4,5)
    a) very fast as there is very little computation required
    b) about the same accuracy as submission
    c) LOAD_DATA (in step 1) should be True
> For Validation make sure the variable in step 1 is set to True. For test use Validation=False. 
